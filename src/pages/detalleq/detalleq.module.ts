import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalleqPage } from './detalleq';

@NgModule({
  declarations: [
    DetalleqPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalleqPage),
  ],
})
export class DetalleqPageModule {}
