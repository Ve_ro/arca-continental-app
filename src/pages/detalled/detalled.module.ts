import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalledPage } from './detalled';

@NgModule({
  declarations: [
    DetalledPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalledPage),
  ],
})
export class DetalledPageModule {}
