import { Component, trigger, state, style, animate, transition } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';

import * as HighCharts from 'highcharts';
/**
 * Generated class for the DetallePage page.
 *
 * See https:ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-detalle',
  templateUrl: 'detalle.html',
  animations: [
     trigger('itemState', [
       state('void', style({backgroundColor: '#5e2129',color:'#fff',  transform: 'scale(1.1)'})),
       state('*', style({ opacity: '1',})),
       transition('void => *', animate('800ms ease-out'))
     ])
   ]
})
export class DetallePage {
	  myDate: String = new Date().toISOString();
	  
	title: string = "RUTINAS DE GESTION";
	tareas:any;
	taskCheched:any[]=[];
	platform:any;
	

	imagenPreview:string;
	constructor(public navCtrl: NavController, private camera: Camera, platform: Platform) {
		this.platform = platform;
	  	this.imagenPreview = null;

	  	this.tareas={
			totalProgress:94,
			task:[
					{
						id:"",
						compleated:false,
						name:"Seguridad(Comíte Regiónal)",
						img:null
					},
					{
						id:"",
						compleated:false,
						name:"Mercadotecnia(todo el eiquipo + Dr.Ro)",
						img:null
					},
					{
						id:"",
						compleated:false,
						name:"Tarjeta ROja",
						img:null
					},
					{
						id:"",
						compleated:false,
						name:"VIVO",
						img:null
					},
					{
						id:"",
						compleated:false,
						name:"Visita Mercado",
						img:null
					},
					{
						id:"",
						compleated:false,
						name:"Cuentas Clave + Mercadotecnia",
						img:null
					},
					{
						id:"",
						compleated:false,
						name:"Entrevista con Jefes de Sección",
						img:null
					}
				]
			}

		

	}

	ngOnInit(){
			var myChart = HighCharts.chart('container1', {
			chart: {
				type: 'solidgauge',
			},
			title: {
			    text: '',
			    style: {
			      color : "#fff"
			    }
			},
			credits: {
                enabled: false
            },
			tooltip: {
				borderWidth:0,
				backgroundColor: 'none',
				shadow: false,
				enabled: false,
				hideDelay:'0',
				style: {
				    fontSize: '0',  
				},
				pointFormat: '',
				positioner: function (labelWidth) {
				    return {
				        x: (this.chart.chartWidth - labelWidth) / 2,
				        y: (this.chart.plotHeight / 2) + 55
				    };
				}
			},
			pane: {
			    startAngle: 360,
		    	endAngle: 0,
			    background: { // Track for Move
			    	backgroundColor:'#999',
			        innerRadius: '110%',
		    		outerRadius: '112%',
			        borderWidth: 1,
			    }
			},
			plotOptions: {
				solidgauge: {
					borderWidth: '0',
				    dataLabels: {
				        enabled: true,
				        borderWidth: 0,
				        y:0,
				    },
				    linecap: 'round',
				    stickyTracking: false,
				    rounded: true
				}
			},
			yAxis: {
					min: 0,
					max: 100,
					lineWidth:0,
					tickPositions: [],
					color : "#999",    
				},
			series: [{
					name: '',
					data: [{
					    color: {
						    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
						    stops: [
						        [0, 'yellow'],
						        [1, 'green']
						    ]
						},
					    y: 44
					}],
					innerRadius: '100%',
					radius: '118%',
					dataLabels: {
					format: '<div style="text-align:center"><span style="font-size:4em;font-weight:bold; color:#000;">{y} % </span></div>'},
				}]
		});
	}

//Se hara un req para actualizar el status de cada tarea.
	checkedTask(i, id) {
        // let index = this.tareas.task[i];
         if (this.tareas.task[i].compleated === false) {
         	this.tareas.task[i].compleated = true;
           	 setTimeout(()=> { 
           	 	let arraysito = this.tareas.task.splice(i, 1); 
           	 	this.taskCheched.push(arraysito[0]);

           	  },10);
        } 
    }
    unCheckedTask(i, id) {
    	console.log(this.taskCheched[i]);
    	if (this.taskCheched[i].compleated === true) {
    		this.taskCheched[i].compleated = false;
    		setTimeout(()=> { 
	    		let arraysito = this.taskCheched.splice(i, 1); 
	    		this.tareas.task.push(arraysito[0]);
    		}, 1000);
    	}        
    }

    //implementar el uso de cámara
    getFoto(id){


    	alert("toma una foto");


    	let options:CameraOptions = {
            quality :100,
            destinationType : this.camera.DestinationType.DATA_URL,
            sourceType : this.camera.PictureSourceType.CAMERA,
            encodingType: this.camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            saveToPhotoAlbum: false
        };

			this.camera.getPicture(options)
				.then((imageData) => {
					 // imageData is either a base64 encoded string or a file URI
					 // If it's base64 (DATA_URL):
					 this.tareas.task[id].img = 'data:image/jpeg;base64,' + imageData;
			}, (err) => {
				console.log("Error en apertura de camara", JSON.stringify(err) );
			 // Handle error
		});

    	/*let options: CameraOptions = {
	      destinationType: this.camera.DestinationType.DATA_URL,
	      targetWidth: 1000,
	      targetHeight: 1000,
	      quality:50,
	      //de donde se extrae la img
	      sourceType:this.camera.PictureSourceType.CAMERA,
	      //para que no la edite
	      allowEdit:false,
	      //para que la regrese en jpg
	      encodingType:this.camera.EncodingType.JPEG,
	      //que no guarde la foto en la galeria del tel

	    }*/

    }
    close(){
     this.platform.exitApp();
  	}

}