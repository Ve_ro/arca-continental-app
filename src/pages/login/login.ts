import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { HomePage } from '../home/home';
import moment from 'moment';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user:any;



  isenabled:boolean=false;

	title: string = "REGISTRO";
	//addingPage = HomePage ;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ViewCtrl: ViewController) {
 
  	this.user = {
      email:"",
      password:""
    }


  }

  

  goHome(){
      //aqui puede cahcar los valores para loguearse
     this.navCtrl.push(HomePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  ngOnInit(){

  }

}
