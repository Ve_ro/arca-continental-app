import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvanceGeneralPage } from './avance-general';

@NgModule({
  declarations: [
    AvanceGeneralPage,
  ],
  imports: [
    IonicPageModule.forChild(AvanceGeneralPage),
  ],
})
export class AvanceGeneralPageModule {}
