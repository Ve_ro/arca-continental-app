import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetallemPage } from './detallem';

@NgModule({
  declarations: [
    DetallemPage,
  ],
  imports: [
    IonicPageModule.forChild(DetallemPage),
  ],
})
export class DetallemPageModule {}
