import { Component, OnInit} from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import  { TAREAS } from "../../data/data.tareas";

import { DetallePage } from '../detalle/detalle';
import { DetalleqPage } from '../detalleq/detalleq';
import { DetallemPage } from '../detallem/detallem';
import { DetalledPage } from '../detalled/detalled';

// import * as HighCharts from 'highcharts';

/*
video 25 y 26
import  { Tasks } from "../../data/data.tareas";
*/


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
	  myDate: String = new Date().toISOString();
	  
	title: string = "RUTINAS DE GESTION";
	tareas:any[] = [];
	platform:any;

	constructor(public navCtrl: NavController, platform: Platform) {
		this.platform = platform;
	  	this.tareas = TAREAS
	}

	ngOnInit(){
		// let task = new Tasks(20,false, "Nombre")
		//tasl.save();

	}

	goDetalle(){
	     //aqui puede cahcar los valores para loguearse
	    this.navCtrl.push(DetallePage);
  	}
  	goDetalleq(){
	     //aqui puede cahcar los valores para loguearse
	    this.navCtrl.push(DetalleqPage);
  	}
  	goDetalleM(){
	     //aqui puede cahcar los valores para loguearse
	    this.navCtrl.push(DetallemPage);
  	}
  	goDetalleD(){
	     //aqui puede cahcar los valores para loguearse
	    this.navCtrl.push(DetalledPage);
  	}

  	close(){
     this.platform.exitApp();
  	}

}
