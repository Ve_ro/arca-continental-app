// lo pimporto
/*
import Dexie from 'dexie';

export class TareasAppDB extends Dexie{
	task:Dexie.Table<ITareas, number>;
	constructor(){
		suport(theTasks);

		this.version(1).stores({
			task:'++id', compleated, name'
		});
		this.task.mapToClass(Tasks);
	}
}

export let db = new TareasAppDB();

export interface ITareas{
	id:number,
	compleated:boolean,
	name:string,
}

export class Tasks imprements ITareas{
	id:number,
	compleated:boolean,
	name:string,

	constructor(id:number, compleated:boolean, name:string){
		this.id = id;
		this.compleated = compleated;
		this.name = name;
	}
	save(){
		return db.task.add(this);
	}
}
*/


export const TAREAS = [
	{
		weekly:[
			{
				totalProgress:94,
				task:[
					{
						id:"",
						compleated:true,
						name:"Seguridad(Comíte Regiónal)",
					},
					{
						id:"",
						compleated:true,
						name:"Mercadotecnia(todo el eiquipo + Dr.Ro)",
					},
					{
						id:"",
						compleated:true,
						name:"Tarjeta ROja",
					},
					{
						id:"",
						compleated:true,
						name:"VIVO",
					},
					{
						id:"",
						compleated:true,
						name:"Visita Mercado",
					},
					{
						id:"",
						compleated:true,
						name:"Cuentas Clave + Mercadotecnia",
					},
					{
						id:"",
						compleated:true,
						name:"Entrevista con Jefes de Sección",
					}
				],
				compleatedTask:[],
			}
		],
		biweekly:[
			{
				totalProgress:94,
				tasks:[
					{
						id:"",
						compleated:true,
						name:"Seguridad(Comíte Regiónal)",
					},
					{
						id:"",
						compleated:true,
						name:"Mercadotecnia(todo el eiquipo + Dr.Ro)",
					},
					{
						id:"",
						compleated:true,
						name:"Tarjeta ROja",
					},
					{
						id:"",
						compleated:true,
						name:"VIVO",
					},
					{
						id:"",
						compleated:true,
						name:"Visita Mercado",
					},
					{
						id:"",
						compleated:true,
						name:"Cuentas Clave + Mercadotecnia",
					},
					{
						id:"",
						compleated:true,
						name:"Entrevista con Jefes de Sección",
					}
				],
				compleatedTask:[],
			}
		],
		monthly:[
			{
				totalProgress:94,
				tasks:[
					{
						id:"",
						compleated:true,
						name:"Seguridad(Comíte Regiónal)",
					},
					{
						id:"",
						compleated:true,
						name:"Mercadotecnia(todo el eiquipo + Dr.Ro)",
					},
					{
						id:"",
						compleated:true,
						name:"Tarjeta ROja",
					},
					{
						id:"",
						compleated:true,
						name:"VIVO",
					},
					{
						id:"",
						compleated:true,
						name:"Visita Mercado",
					},
					{
						id:"",
						compleated:true,
						name:"Cuentas Clave + Mercadotecnia",
					},
					{
						id:"",
						compleated:true,
						name:"Entrevista con Jefes de Sección",
					}
				],
				compleatedTask:[],
			}
		],
		diverse:[
			{
				totalProgress:94,
				tasks:[
					{
						id:"",
						compleated:true,
						name:"Seguridad(Comíte Regiónal)",
					},
					{
						id:"",
						compleated:true,
						name:"Mercadotecnia(todo el eiquipo + Dr.Ro)",
					},
					{
						id:"",
						compleated:true,
						name:"Tarjeta ROja",
					},
					{
						id:"",
						compleated:true,
						name:"VIVO",
					},
					{
						id:"",
						compleated:true,
						name:"Visita Mercado",
					},
					{
						id:"",
						compleated:true,
						name:"Cuentas Clave + Mercadotecnia",
					},
					{
						id:"",
						compleated:true,
						name:"Entrevista con Jefes de Sección",
					}
				],
				compleatedTask:[],
			}
		],

	}
]