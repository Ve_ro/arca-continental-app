import { Directive,ElementRef, Renderer } from '@angular/core';

/**
 * Generated class for the HeaderDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[header]' // Attribute selector
})
export class HeaderDirective {

  constructor(public element:ElementRef, public renderer:Renderer) {
    console.log('Hello HeaderDirective Directive');
  }

}
