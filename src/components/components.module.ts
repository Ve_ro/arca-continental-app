import { NgModule } from '@angular/core';
import { GraphCircularComponent } from './graph-circular/graph-circular';
@NgModule({
	declarations: [GraphCircularComponent],
	imports: [],
	exports: [GraphCircularComponent]
})
export class ComponentsModule {}
