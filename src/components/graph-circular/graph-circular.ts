import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as HighCharts from 'highcharts';

/**
 * Generated class for the GraphCircularComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'graph-circular',
  templateUrl: 'graph-circular.html'
})
export class GraphCircularComponent {

	text: string;
	chartOptions : any;

	constructor() {
	    console.log('Hello GraphCircularComponent Component');
	    this.text = 'GRACICA';
	}


	ngOnInit(){
			var myChart = HighCharts.chart('container', {
			chart: {
				type: 'solidgauge',
			},
			title: {
			    text: '',
			    style: {
			      color : "#fff"
			    }
			},
			credits: {
                enabled: false
            },
			tooltip: {
				borderWidth:0,
				backgroundColor: 'none',
				shadow: false,
				enabled: false,
				hideDelay:'0',
				style: {
				    fontSize: '0',  
				},
				pointFormat: '',
				positioner: function (labelWidth) {
				    return {
				        x: (this.chart.chartWidth - labelWidth) / 2,
				        y: (this.chart.plotHeight / 2) + 55
				    };
				}
			},
			pane: {
			    startAngle: 360,
		    	endAngle: 0,
			    background: { // Track for Move
			    	backgroundColor:'#999',
			        innerRadius: '110%',
		    		outerRadius: '112%',
			        borderWidth: 1,
			    }
			},
			plotOptions: {
				solidgauge: {
					borderWidth: '0',
				    dataLabels: {
				        enabled: true,
				        borderWidth: 0,
				        y:0,
				    },
				    linecap: 'round',
				    stickyTracking: false,
				    rounded: true
				}
			},
			yAxis: {
					min: 0,
					max: 100,
					lineWidth:0,
					tickPositions: [],
					color : "#999",    
				},
			series: [{
					name: '',
					data: [{
					    color: {
						    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
						    stops: [
						        [0, '#fc0831'],
						        [1, '#7e000f']
						    ]
						},
					    y: 44
					}],
					innerRadius: '100%',
					radius: '118%',
					dataLabels: {
					format: '<div style="text-align:center"><span style="font-size:4em;font-weight:bold; color:#000;">{y} % </span></div>'},
				}]
		});
	}
}
