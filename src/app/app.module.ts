import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


/*
para importar la BD
import db from '../data/data.tareas'; */


import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { DetallePage } from '../pages/detalle/detalle';
import { DetalleqPage } from '../pages/detalleq/detalleq';
import { DetallemPage } from '../pages/detallem/detallem';
import { DetalledPage } from '../pages/detalled/detalled';

import { HeaderDirective } from '../directives/header/header';
//import { GraficaCircularDirective } from '../directives/grafica-circular/grafica-circular';
import { GraphCircularComponent } from '../components/graph-circular/graph-circular';
 
//pluguin
import { Camera } from '@ionic-native/camera';


@NgModule({
  declarations: [
    MyApp,
    HeaderDirective,
    HomePage,
    LoginPage,
    DetallePage,
    DetalleqPage,
    DetallemPage,
    DetalledPage,

    // Directivas
    //GraficaCircularDirective,
    //Component
    GraphCircularComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    DetallePage,
    DetalleqPage,
    DetallemPage,
    DetalledPage,
  ],
  providers: [
    StatusBar,
    Camera,
    {provide: ErrorHandler,  useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
