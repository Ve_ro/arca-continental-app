import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { LoginPage } from '../pages/login/login';
//  import { HomePage } from '../pages/home/home';
// import { DetallePage } from '../pages/detalle/detalle';

import solidgauge from 'highcharts/modules/solid-gauge';
import more from 'highcharts/highcharts-more';
import * as Highcharts from 'highcharts';
more(Highcharts);
solidgauge(Highcharts);


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
    });
  }
}

